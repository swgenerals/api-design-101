# API Design 101: 기초부터 모범 사례까지 <sup>[1](#footnote_1)</sup>

![](./images/1_vWvkkgG6uvgmJT8GkId98A.webp)

이 포스팅에서는 기본부터 시작하여 뛰어난 API를 정의하는 모범 사례로 나아가는 API 설계를 살펴볼 것이다.

개발자로서 이러한 개념 중 상당수는 이미 잘 알고 있겠지만, 이해를 돕기 위해 자세히 설명하고자 한다.

<a name="footnote_1">1</a>: 이 페이지는 [API Design 101: From Basics to Best Practices](https://levelup.gitconnected.com/api-design-101-from-basics-to-best-practices-a0261cdf8886)를 편역한 것이다.
